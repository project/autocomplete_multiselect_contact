Autocomplete Multiselect Cc and BCc Users Contact form
=========

autocomplete_multiselect_contact provides CC and BCC recipient fields in the 
site contact forms with autocomplete and multiselect functionality.This module 
enables to cc and bcc by including user profiles created in the People section
in the drupal setup.



Dependencies
============

* Contact


Installation and Usage
======================

1) Copy the autocomplete_multiselect_cc folder to the modules folder in your 
installation.

2) Enable the module (/admin/modules).

3) Go in Structure Contact Forms and add new Contact Form.After adding a Contact
 form when you edit it,you will find 2 new fields called Autoselect CC and 
Autoselect BCC fields.

4) Enter the Username letter alphabet and you will see the list of users 
starting the letter or word entered.You can multiselect the users from the 
    given list and also can remove them easily if not required.

5)The selected Users in CC and BCC will receive the email.

